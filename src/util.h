#ifndef UTIL_H
#define UTIL_H

#include <stdint.h>

#define ADD_UNDERSCORE_0(x,y) x ## _ ## y
#define ADD_UNDERSCORE(x,y) ADD_UNDERSCORE_0(x,y)
#define STRINGIFY_0(s) #s
#define STRINGIFY(s) STRINGIFY_0(s)

uint32_t roundp2(uint32_t v);

#endif
