#include "vec.h"

void vec_free(vec_t *vec) {
    vec->vtable->free(vec);
}

int vec_reserve(vec_t *vec, uint32_t count) {
    return vec->vtable->reserve(vec, count);
}

int vec_push(vec_t *vec, void *value) {
    return vec->vtable->push(vec, value);
}

void vec_dbg(vec_t *vec) {
    vec->vtable->dbg(vec);
}
