#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include "util.h"

#ifndef VEC_H
#define VEC_H

struct vec_vtable;

typedef struct {
    void *data;
    uint32_t length;
    uint32_t capacity;
    struct vec_vtable *vtable;
} vec_t;

struct vec_vtable {
    void (*free)(vec_t *vec);
    int (*reserve)(vec_t *vec, uint32_t count);
    int (*push)(vec_t *vec, void *value);
    void (*dbg)(vec_t *vec);
};

void vec_free(vec_t *vec);
int vec_reserve(vec_t *vec, uint32_t count);
int vec_push(vec_t *vec, void *value);
void vec_dbg(vec_t *vec);

#endif

#ifdef DTYPE

#define VEC ADD_UNDERSCORE(vec, DNAME)
typedef struct {
    DTYPE *data;
    uint32_t length;
    uint32_t capacity;
} VEC;

#define VEC_NEW ADD_UNDERSCORE(VEC, new)
void VEC_NEW(VEC *vec) {
    vec->length = 0;
    vec->capacity = 0;
}

#define VEC_FREE ADD_UNDERSCORE(VEC, free)
void VEC_FREE(VEC *vec) {
    if (vec->capacity > 0)
        free(vec->data);
}

#define VEC_RESERVE ADD_UNDERSCORE(VEC, reserve)
int VEC_RESERVE(VEC *vec, uint32_t count) {
    size_t new_cap = vec->length + count;
    if (new_cap < vec->capacity) return 1;

    new_cap = roundp2(new_cap);

    if (vec->capacity == 0)
        vec->data = malloc(new_cap * sizeof(DTYPE));
    else
        vec->data = realloc(vec->data, new_cap * sizeof(DTYPE));

    vec->capacity = new_cap;

    if (vec->data == NULL)
        return 0;

    return 1;
}

#define VEC_PUSH ADD_UNDERSCORE(VEC, push)
int VEC_PUSH(VEC *vec, DTYPE value) {
    if (!VEC_RESERVE(vec, 1)) return 0;
    vec->data[vec->length++] = value;
    return 1;
}

#define VEC_DBG ADD_UNDERSCORE(VEC, dbg)
void VEC_DBG(VEC *vec) {
    fprintf(
        stderr,
        "vec<" STRINGIFY(DTYPE) "> (vec_" STRINGIFY(DNAME) ") len: %u, cap: %u; [",
        vec->length, vec->capacity
    );

    for (uint32_t i = 0; i < vec->length; i++) {
        if (i > 0) fprintf(stderr, ", ");
        fprintf(stderr, DFORMAT, vec->data[i]);
    }

    fprintf(stderr, "]\n");
}

#define VEC_VPUSH ADD_UNDERSCORE(VEC, vpush)
static int VEC_VPUSH(vec_t *vec, void *value) {
    return VEC_PUSH((VEC *) vec, *(DTYPE *) value);
}

#define VEC_VTABLE ADD_UNDERSCORE(VEC, vtable)
static struct vec_vtable VEC_VTABLE = {
    .free = (void (*)(vec_t *)) VEC_FREE,
    .reserve = (int (*)(vec_t *, uint32_t)) VEC_RESERVE,
    .push = (int (*)(vec_t *, void *)) VEC_VPUSH,
    .dbg = (void (*)(vec_t *)) VEC_DBG
};

#define VEC_VIRT ADD_UNDERSCORE(VEC, virt)
vec_t VEC_VIRT(VEC *vec) {
    return (vec_t) {
        .data = vec->data,
        .length = vec->length,
        .capacity = vec->capacity,
        .vtable = &VEC_VTABLE
    };
}

#undef DNAME
#undef DTYPE
#undef DFORMAT

#endif
