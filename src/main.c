#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

#define DTYPE int
#define DNAME int
#define DFORMAT "%d"
#include "vec.h"

#define DTYPE char *
#define DNAME str
#define DFORMAT "%s"
#include "vec.h"

int main(void) {
    vec_int vec_i;
    vec_str vec_s;
    vec_t vec;

    vec_int_new(&vec_i);
    vec_str_new(&vec_s);

    for (int i = 0; i < 100; i++) {
        vec_int_push(&vec_i, i);
    }

    vec_int_dbg(&vec_i);

    for (int i = 0; i < 10; i++) {
        char *str = malloc(32);
        sprintf(str, "hello#%d", i);
        vec_str_push(&vec_s, str);
    }

    vec_str_dbg(&vec_s);

    vec = vec_int_virt(&vec_i);
    vec_dbg(&vec);  // dynamic dispatch!

    for (uint32_t i = 0; i < vec_s.length; i++) {
        free(vec_s.data[i]);
    }

    vec_int_free(&vec_i);
    vec_str_free(&vec_s);

    return EXIT_SUCCESS;
}
