CC = gcc
CFLAGS = -Wall -Wextra -std=c99 -O1 -g

SOURCES = $(wildcard **/*.c)
OBJECTS = $(SOURCES:.c=.o)

j4f: $(OBJECTS)
	$(CC) $(LDFLAGS) $(OBJECTS) $(LDLIBS) -o j4f

clean:
	rm -f ./**/*.o
